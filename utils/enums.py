from enum import Enum


class Color(Enum):
    Black = 0
    White = 1


class Piece(Enum):
    Queen = 0
    King = 1
    Rook = 2
    Bishop = 3
    Knight = 4
    Pawn = 5


class Status(Enum):
    Enabled = 0
    Warning = 1
    Disabled = 2
