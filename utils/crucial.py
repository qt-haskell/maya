import os
import aiofiles
import discord


# Counts the total lines
async def total_lines(path: str, filetype: str = ".py"):
    lines = 0
    for i in os.scandir(path):
        if i.is_file():
            if i.path.endswith(filetype):
                lines += len((await (await aiofiles.open(i.path, "r", encoding="utf-8")).read()).split("\n"))
        elif i.is_dir():
            lines += await total_lines(i.path, filetype)
    return lines


# Counts others [ classes and functions ]
async def misc(path: str, filetype: str = ".py", file_has: str = "def"):
    count_lines = 0
    for i in os.scandir(path):
        if i.is_file():
            if i.path.endswith(filetype):
                count_lines += len(
                    [line for line in (await (await aiofiles.open(i.path, "r", encoding="utf-8")).read()).split("\n") if
                     file_has in line])
        elif i.is_dir():
            count_lines += await misc(i.path, filetype, file_has)
    return count_lines


# Make a webhook if its own is not present.
async def fetch_webhook(channel) -> discord.Webhook:
    if isinstance(channel, discord.Thread):
        channel = channel.parent
    webhook_list = await channel.webhooks()
    if webhook_list:
        for hook in webhook_list:
            if hook.token:
                return hook
    webhook = await channel.create_webhook(
        name="Lichess Webhook",
        avatar=await channel.guild.me.display_avatar.read())
    return webhook
