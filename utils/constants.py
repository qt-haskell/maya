import discord

from discord import Object

chessPieces = {
    "black_rook_black": "<:_:945769292085989377>",
    "black_rook_white": "<:_:945769290659930122>",
    "black_pawn_black": "<:_:945769290282434592>",
    "black_pawn_white": "<:_:945769290802557008>",
    "black_bishop_black": "<:_:945769290865455154>",
    "black_bishop_white": "<:_:945769291024830515>",
    "black_knight_black": "<:_:945769290936774756>",
    "black_knight_white": "<:_:945769290857086996>",
    "black_queen_black": "<:_:945769290655740015>",
    "black_queen_white": "<:_:945769290748014662>",
    "black_king_black": "<:_:945769290991292436>",
    "black_king_white": "<:_:945769290810925196>",
    "white_pawn_black": "<:_:945769290777378836>",
    "white_pawn_white": "<:_:945769290676715600>",
    "white_bishop_black": "<:_:945769290911600702>",
    "white_bishop_white": "<:_:945769290714468412>",
    "white_knight_black": "<:_:945769290425040907>",
    "white_knight_white": "<:_:945769290425053285>",
    "white_rook_black": "<:_:945769290806730752>",
    "white_rook_white": "<:_:945769290563481651>",
    "white_king_black": "<:_:945769290685120534>",
    "white_king_white": "<:_:945769290718654565>",
    "white_queen_black": "<:_:945769290768994355>",
    "white_queen_white": "<:_:945769290492149801>",
    "blank_black": "<:_:945770523332345886>",
    "blank_white": "<:_:945770523449757696>",
}


class constants:
    default_color = discord.Color.blurple()
    testing_guild = Object(id=944617327264137327)
    sod_guild = Object(id=1000747308591366165)
    developers = [546691865374752778, 852419718819348510]
    bot_version = '0.0.1'
    discord_version = discord.version_info
    accept = '<:Accepted:997509038982041650>'
    decline = '<:Declined:997509040517169204>'
    payload_list = ['Accepted', 'Declined']
    tag_db = 'tag_database'
    tag_db_path = '/database/tag_database.db'

