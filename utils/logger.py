import os
import inspect


def log(o: object) -> object:
    curframe = inspect.currentframe()
    calframe = inspect.getouterframes(curframe, 2)

    print("-------------------------------")
    print("🔰", f"[{calframe[1][1].split('Lichess-Discord-Bot')[1][1:]}]", o)
    print("-------------------------------")
