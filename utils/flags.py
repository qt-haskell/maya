import discord


# Has required flags for Server and User Info Commands
def user_perms(permissions):
    perms_list = []
    if permissions.administrator:
        perms_list.append("Admin")
        return ["Admin"]

    if permissions.manage_guild:
        perms_list.append("Manage Guild")

    if permissions.ban_members:
        perms_list.append("Ban Guild Members")

    if permissions.kick_members:
        perms_list.append("Kick Guild Members")

    if permissions.manage_channels:
        perms_list.append("Manage Channels")

    if permissions.manage_emojis:
        perms_list.append("Manage Emojis")

    if permissions.manage_permissions:
        perms_list.append("Manage Member Permissions")

    if permissions.manage_roles:
        perms_list.append("Manage Member Roles")

    if permissions.mention_everyone:
        perms_list.append("Ping @everyone")

    if permissions.mute_members:
        perms_list.append("Mute Members")

    if permissions.deafen_members:
        perms_list.append("Deafen Members")

    if permissions.view_audit_log:
        perms_list.append("View the Audit Log")

    if permissions.manage_webhooks:
        perms_list.append("Mange Webhooks")

    if permissions.create_instant_invite:
        perms_list.append("Create Instant Invites")

    if len(perms_list) == 0:
        return None
    return perms_list


def user_badges(user: discord.User, fetch_user):
    user_flags = user.public_flags
    flags = dict(user_flags)
    flags_emote = ""

    if flags["staff"] is True:
        flags_emote = f"{flags_emote} `Staff`"

    if flags["partner"] is True:
        flags_emote = f"{flags_emote} `Partner`"

    if flags["hypesquad"] is True:
        flags_emote = f"{flags_emote} `Hypesquad`"

    if flags["bug_hunter"] is True:
        flags_emote = f"{flags_emote} `Bug Hunter`"

    if flags["hypesquad_bravery"] is True:
        flags_emote = f"{flags_emote} `Hypesquad Bravery`"

    if flags["hypesquad_brilliance"] is True:
        flags_emote = f"{flags_emote} `Hypesquad Brilliance`"

    if flags["hypesquad_balance"] is True:
        flags_emote = f"{flags_emote} `Hypesquad Balance`"

    if flags["early_supporter"] is True:
        flags_emote = f"{flags_emote} `Early Supporter`"

    if user.premium_since:
        flags_emote = f"{flags_emote} `Nitro Subscriber`"

    if user.premium_since:
        flags_emote = f"{flags_emote} `Server Booster`"

    if flags["verified_bot_developer"] is True:
        flags_emote = f"{flags_emote} `Early Verified Bot Dev.`"

    if flags_emote == "": flags_emote = None

    return flags_emote