from bs4 import BeautifulSoup as soup
from utils.constants import chessPieces

import aiohttp


class RateLimited(Exception):
    pass


class Client:
    def __init__(self):
        self.base = 'https://lichess.org/api'
        self.session = aiohttp.ClientSession()
        self.timeout = 10
        self.errors = {429: RateLimited}

    async def _request(self, method, endpoint, data):
        url = self.base + endpoint
        async with self.session.request(method, url, json=data, timeout=self.timeout) as r:
            if r.status in list(self.errors.keys()):
                raise self.errors.get(r.status)
            return await r.json()

    async def get(self, endpoint):
        respond = await self._request("GET", endpoint, None)
        return respond

    async def post(self, endpoint, data):
        respond = await self._request("POST", endpoint, data)
        return respond

    async def put(self, endpoint, data):
        respond = await self._request("PUT", endpoint, data)
        return respond

    async def delete(self, endpoint):
        respond = await self._request("DELETE", endpoint, None)
        return respond


class LichessHTTP:
    def __init__(self):
        self.client = Client()

    async def create_open_challenge(self, varient):
        endpoint = "/challenge/open"
        body = {"varient": varient}
        respond = await self.client.post(endpoint, body)
        return respond


def chessDotComBoard(_id: str, side: str = 'white'):
    pass


async def getLichessBoard(_id: str, side: str = 'white'):
    try:
        url = 'https://lichess.org/{0}/{1}'.format(_id, side)
        async with aiohttp.ClientSession() as cs:
            async with cs.get(url) as r:
                text = await r.read()
                pagesoup = soup(text, 'html.parser')

                #player_one = pagesoup.text.split('[White "')[1]
                #player_one = player_one.split('"]')[0]
                #player_one = 'White: ' + player_one
                #player_two = pagesoup.text.split('[Black "')[1]
                #player_two = player_two.split('"]')[0]
                #player_two = 'Black: ' + player_two

                pieces = pagesoup.find_all('piece')

                if not pieces:
                    gifUri = f'https://lichess1.org/game/export/gif/{side}/{_id}.gif'

                    return True, True, gifUri# player_one, player_two

                board = [[chessPieces["blank" + ("_white" if (i + j) % 2 == 0 else "_black")] for i in range(8)] for j in range(8)]

                for piece in pieces:
                    pieceStyle = piece.attrs["style"]
                    y, x = pieceStyle.split(";")
                    y = int(float(y.split(":")[-1][:-1]) // 12.5)
                    x = int(float(x.split(":")[-1][:-1]) // 12.5)
                    pieceName = "_".join(piece.attrs["class"]) + ("_white" if (x + y) % 2 == 0 else "_black")

                    board[y][x] = chessPieces[pieceName]

                    formattedBoard = "\n".join(map(lambda x: "".join(x), board))

                return True, False, formattedBoard#, player_one, player_two

    except Exception as exc:
        print(exc)
        return False, True, exc#, None, None
