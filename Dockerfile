
FROM 3.10.5-slim-buster
 
WORKDIR /source 
COPY requirements.txt requirements.txt 

RUN pip3 install -r requirements.txt 
COPY . . 

CMD [ "python3.exe", "--version"]
