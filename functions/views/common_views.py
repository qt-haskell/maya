import discord


class ConfirmView(discord.ui.View):
    def __init__(self, userID: int, otherID: int = None):
        super().__init__()
        self.userID = userID
        self.otherID = otherID
        self.value = None

    @discord.ui.button(label='Confirm', style=discord.ButtonStyle.green)
    async def confirm(self, interaction: discord.Interaction, button: discord.ui.Button):

        if not self.userID or interaction.user.id == self.userID:
            self.value = True
            self.stop()

        else:
            await interaction.response.send_message(
                "This interaction is not for you!", ephemeral=True)

    @discord.ui.button(label='Cancel', style=discord.ButtonStyle.red)
    async def cancel(self, interaction: discord.Interaction, button: discord.ui.Button):

        if not self.userID or interaction.user.id == self.userID or interaction.user.id == self.otherID:
            self.value = False
            self.stop()
        else:
            await interaction.response.send_message(
                "This interaction is not for you!", ephemeral=True)


class DeleteView(discord.ui.View):
    def __init__(self, ctx):
        super().__init__()
        self.message = None
        self.ctx = ctx

    @discord.ui.button(label='Cancel', style=discord.ButtonStyle.red)
    async def cancel(self, button, interaction):
        await self.message.delete()
        self.stop()

    async def interaction_check(self, interaction):
        if interaction.user != self.ctx.author:
            await interaction.response.send_message("This is not your interaction!", ephemeral=True)
            return False

        return True
