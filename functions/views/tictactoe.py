import discord


class ttt_button(discord.ui.Button["TicTacToe"]):

    def __init__(self, x: int, y: int):
        super().__init__(style=discord.ButtonStyle.secondary, label="\u200b", row=y)
        self.x = x
        self.y = y
        self.used = False
        self.xplayer = None
        self.oplayer = None

    async def callback(self, interaction: discord.Interaction):
        view: ttt_game = self.view
        if self.used:
            return
        valid = view.check_if_valid(self, interaction)
        if not valid:
            return
        self.used = True
        state = view.board[self.y][self.x]
        if state in (view.X, view.O):
            return

        if view.current_player == view.X:
            self.style = discord.ButtonStyle.danger
            self.label = "X"
            self.disabled = True
            view.board[self.y][self.x] = view.X
            view.current_player = view.O
            if view.oplayer is not None:
                player = view.oplayer.mention
            else:
                player = "O"
            content = f"<:blurpleannouncements:1000537031849562123> It is now {player}'s turn"
        else:
            self.style = discord.ButtonStyle.success
            self.label = "O"
            self.disabled = True
            view.board[self.y][self.x] = view.O
            view.current_player = view.X
            if view.xplayer is not None:
                player = view.xplayer.mention
            else:
                player = "X"
            content = f"<:blurpleannouncements:1000537031849562123> It is now {player}'s turn"

        winner = view.check_board_winner()
        if winner is not None:
            if winner == view.X:
                content = f"<:blurpleannouncements:1000537031849562123> {view.xplayer.mention} won!"
            elif winner == view.O:
                content = f"<:blurpleannouncements:1000537031849562123> {view.oplayer.mention} won!"
            else:
                content = "<:blurpleannouncements:1000537031849562123> It's a tie!"

            for child in view.children:
                assert isinstance(child, discord.ui.Button)  # just to shut up the linter
                child.disabled = True

            view.stop()
        self.used = False

        await interaction.response.edit_message(content=content, view=view)


class ttt_game(discord.ui.View):
    X = -1
    O = 1
    Tie = 2

    def __init__(self, xplayer, oplayer):
        super().__init__()
        self.current_player = self.X
        self.board = [
            [0, 0, 0],
            [0, 0, 0],
            [0, 0, 0],
        ]

        for x in range(3):
            for y in range(3):
                self.add_item(ttt_button(x, y))
        self.xplayer, self.oplayer = xplayer, oplayer

    def check_if_valid(self, button, interaction):
        author = interaction.user
        if self.current_player == self.X:
            if self.xplayer is None:
                self.xplayer = author
            if author != self.xplayer:
                return False
        else:
            if self.oplayer is None:
                if author == self.xplayer:
                    return False
                self.oplayer = author
            if author != self.oplayer:
                return False
        return True

    def check_board_winner(self):
        for across in self.board:
            value = sum(across)
            if value == 3:
                return self.O
            elif value == -3:
                return self.X

        for line in range(3):
            value = self.board[0][line] + self.board[1][line] + self.board[2][line]
            if value == 3:
                return self.O
            elif value == -3:
                return self.X

        diag = self.board[0][2] + self.board[1][1] + self.board[2][0]
        if diag == 3:
            return self.O
        elif diag == -3:
            return self.X

        diag = self.board[0][0] + self.board[1][1] + self.board[2][2]
        if diag == 3:
            return self.O
        elif diag == -3:
            return self.X

        if all(i != 0 for row in self.board for i in row):
            return self.Tie

        return None