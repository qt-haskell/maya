from discord.ext import commands

import discord
import contextlib

from functions.views.common_views import ConfirmView
from difflib import get_close_matches


class HelpEmbed(discord.Embed):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        text = "Use help [command] or help [category] for more information | <> is required | [] is optional"
        self.set_footer(text=text)
        self.color = discord.Color.blurple()


class MyHelp(commands.HelpCommand):
    def __init__(self):
        super().__init__(
            command_attrs={
                "help": "The help command for the bot",
                "aliases": ['commands']
            }
        )

    async def send(self, **kwargs):
        await self.get_destination().send(**kwargs)

    async def command_not_found(self, string: str, /) -> str:
        raw_commands = []

        for command in self.context.bot.walk_commands():
            if not command.hidden:
                raw_commands += (command.name, *command.aliases)

        for cog in self.context.bot.cogs:
            raw_commands += (cog, *['No Desc', 'No description'])

        if similar_command_data := get_close_matches(string, raw_commands, 1):
            similar_command_data = similar_command_data[0]
            similar_command = self.context.bot.get_command(similar_command_data)

            if not similar_command:
                if similar_cog_data := get_close_matches(string, raw_commands, 1):
                    similar_cog_data = similar_cog_data[0]
                    similar_cog = self.context.bot.get_cog(similar_cog_data)

                    view = ConfirmView(self.context.message.author.id)
                    misspelled_content = self.context.message.content

                    suggestion_string = str(similar_cog)
                    suggestion_string = suggestion_string.split('cogs.')[1]
                    suggestion_string = suggestion_string.split('.')[0]

                    if not similar_cog:
                        return f'No cog named `{string}` found.'

                    e = discord.Embed(title='Command not found.', color=discord.Color.blurple())
                    e.add_field(name='Command Suggestion',
                                value=misspelled_content.replace(string, suggestion_string, 1))

                    context = await self.context.send(embed=e, view=view)

                    await view.wait()

                    if view.value:
                        await context.delete()
                        await self.context.send_help(similar_cog)
                    else:
                        await context.delete()
                else:
                    return f'No command named `{string}` found.'
            else:
                view = ConfirmView(self.context.message.author.id)
                misspelled_content = self.context.message.content
                suggestion_string = str(similar_command)

                e = discord.Embed(title='Command not found.', color=discord.Color.blurple())
                e.add_field(name='Command Suggestion', value=misspelled_content.replace(string, suggestion_string, 1))

                context = await self.context.send(embed=e, view=view)

                await view.wait()

                if view.value:
                    await context.delete()
                    await self.context.send_help(similar_command)
                else:
                    await context.delete()

    async def send_bot_help(self, mapping):
        ctx = self.context
        embed = HelpEmbed(title=f"{ctx.me.display_name} help")
        embed.set_thumbnail(url=ctx.me.avatar)
        usable = 0

        for cog, attrs in mapping.items():
            if filtered_commands := await self.filter_commands(attrs):
                amount_commands = len(filtered_commands)
                usable += amount_commands
                if cog:
                    name = cog.display_name  # custom property for a better display
                    description = cog.description or "No description"
                else:
                    name = "<:blurplesettings:1000373596759265290> help"
                    description = f"<:replyarrow:1000374996612436039> Default help commands"

                embed.add_field(name=f"{name} category [{amount_commands}]", value=description, inline=False)

        embed.description = f"{len(self.context.bot.commands)} commands | {usable} usable"

        try:
            await self.send(embed=embed)
        except discord.Forbidden:
            try:
                await self.context.send('Hey, seems like I can\'t send embeds.')
            except discord.Forbidden:
                await self.context.author.send(
                    f'Hey, seems like I can\'t send any message in {self.context.channel.name} on {self.context.guild.name}\n'
                    f'May you inform the server team about this issue? :slight_smile:')

    async def get_command_help(self, attrs):
        signature = self.get_command_signature(attrs)
        embed = HelpEmbed(title=signature, description=attrs.help or "No help found...")

        if cog := attrs.cog:
            embed.add_field(name="category", value=cog.display_name)

        can_run = "No"
        with contextlib.suppress(commands.CommandError):
            if await attrs.can_run(self.context):
                can_run = "Yes"

        embed.add_field(name="Usable", value=can_run)
        return embed

    async def send_command_help(self, command):
        signature = self.get_command_signature(command)
        embed = HelpEmbed(title=signature, description=command.help or "No help found...")

        if cog := command.cog:
            embed.add_field(name="category", value=cog.display_name)

        can_run = "No"
        with contextlib.suppress(commands.CommandError):
            if await command.can_run(self.context):
                can_run = "Yes"

        embed.add_field(name="Usable", value=can_run)

        try:
            await self.send(embed=embed)
        except discord.Forbidden:
            try:
                await self.context.send('Hey, seems like I can\'t send embeds.')
            except discord.Forbidden:
                await self.context.author.send(
                    f'Hey, seems like I can\'t send any message in {self.context.channel.name} on {self.context.guild.name}\n'
                    f'May you inform the server team about this issue? :slight_smile:')

    async def send_help_embed(self, title, description, attrs):
        embed = HelpEmbed(title=title, description=description or "No help found...")

        if filtered_commands := await self.filter_commands(attrs):
            for command in filtered_commands:
                embed.add_field(name=self.get_command_signature(command), value=command.help or "No help found...", inline=False)
        try:
            await self.send(embed=embed)
        except discord.Forbidden:
            try:
                await self.context.send('Hey, seems like I can\'t send embeds.')
            except discord.Forbidden:
                await self.context.author.send(
                    f'Hey, seems like I can\'t send any message in {self.context.channel.name} on {self.context.guild.name}\n'
                    f'May you inform the server team about this issue? :slight_smile:')

    async def send_group_help(self, group):
        title = self.get_command_signature(group)
        try:
            await self.send_help_embed(title, group.help, group.commands)
        except discord.Forbidden:
            try:
                await self.context.send('Hey, seems like I can\'t send embeds.')
            except discord.Forbidden:
                await self.context.author.send(
                    f'Hey, seems like I can\'t send any message in {self.context.channel.name} on {self.context.guild.name}\n'
                    f'May you inform the server team about this issue? :slight_smile:')

    async def send_cog_help(self, cog):
        title = cog.display_name or "No Name?"  # custom property for a better display
        try:
            await self.send_help_embed(f'{title} category', cog.description, cog.get_commands())
        except discord.Forbidden:
            try:
                await self.context.send('Hey, seems like I can\'t send embeds.')
            except discord.Forbidden:
                await self.context.author.send(
                    f'Hey, seems like I can\'t send any message in {self.context.channel.name} on {self.context.guild.name}\n'
                    f'May you inform the server team about this issue? :slight_smile:')
