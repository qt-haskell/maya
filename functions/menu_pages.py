import discord
from discord.ext import menus

from functions.structure import menuEmbed
from functions.structure import user_friendly_dt

linked = '<:link:998269960856027156>'
category = '<:cat:998269957349580912>'
thread = '<:thread:998269959148933210>'
arrow = '<:arrow:998269955432775700>'
star = '<:star:998269954002522213>'


class HelpPages(menus.MenuPages, inherit_buttons=False):
    @menus.button(linked, position=menus.First(1))
    async def go_before(self, payload):
        await self.show_checked_page(self.current_page - 1)

    @menus.button(category, position=menus.Last(0))
    async def go_after(self, payload):
        await self.show_checked_page(self.current_page + 1)

    @menus.button(thread, position=menus.First(0))
    async def go_first(self, payload):
        await self.show_page(0)

    @menus.button(arrow, position=menus.Last(1))
    async def go_last(self, payload):
        await self.show_page(self._source.get_max_pages() - 1)

    @menus.button(star, position=menus.First(2))
    async def go_stop(self, payload):
        self.stop()


class WastebasketMenu(menus.MenuPages):
    @menus.button('\N{WASTEBASKET}\ufe0f', position=menus.Last(3))
    async def do_trash(self, _):
        self.stop()
        await self.message.delete()

    def stop(self):
        self.call_end_event()
        super().stop()

    async def finalize(self, timed_out):
        self.call_end_event()

    def call_end_event(self):
        self.bot.dispatch('finalize_menu', self.ctx)


class SnipeMenu(menus.ListPageSource):
    def __init__(self, entries, first_message: discord.Embed = None):
        if first_message:
            self.num_offset = 1
            entries[:0] = [first_message]
        else:
            self.num_offset = 0
        super().__init__(entries, per_page=1)

    async def format_page(self, menu, entries):
        if isinstance(entries, discord.Embed): return entries

        index = menu.current_page + 1 - self.num_offset
        message = entries

        reply = message.reference
        if reply:
            reply = reply.resolved
        reply_rm = isinstance(reply, discord.DeletedReferencedMessage)

        embed = menuEmbed(
            menu.ctx.author,
            title=f'Sniped message {index}/{self._max_pages - self.num_offset}:',
            description=f'{message.content}'
        )
        embed.set_author(name=f'{message.author}: {message.author.id}', icon_url=message.author.avatar.url)

        if message.attachments:
            if message.attachments[0].filename.endswith(('png', 'jpg', 'jpeg', 'gif', 'webp')):
                embed.set_image(url=message.attachments[0].proxy_url)

            file_urls = [f'[{file.filename}]({file.proxy_url})' for file in message.attachments]
            embed.add_field(name='Deleted files:', value=f'\n'.join(file_urls))

        embed.add_field(name=f'Message created at:', value=user_friendly_dt(message.created_at),
                        inline=False)
        if reply:
            if reply_rm:
                msg = 'Replied message has been deleted.'
            else:
                msg = f'Replied to {reply.author} - [Link to replied message]({reply.jump_url} "Jump to Message")'
            embed.add_field(name='Message reply:', value=msg)

        embed.add_field(name='Message channel:', value=message.channel.mention, inline=False)
        return embed

