from typing import Optional, Literal
from discord import Color, Embed
from discord.ext import commands

import re
import datetime
import discord


class struct:
    TimestampStyle = Literal['f', 'F', 'd', 'D', 't', 'T', 'R']


class fmt_time(object):
    def __init__(self, unit_name, unit_time, amount_units, total_seconds):
        self.unit_name = unit_name
        self.unit_time = unit_time
        self.amount_units = amount_units
        self.total_seconds = total_seconds


class customEmbed(Embed):
    def __init__(self, *args, **kwargs):
        if 'color' not in kwargs:
            kwargs['color'] = Color.blurple()
        super().__init__(*args, **kwargs)
        self.set_footer(
            text='Lichess discord bot'
        )


def cleanup_code(content) -> str:
    if content.startswith('```') and content.endswith('```'):
        return '\n'.join(content.split('\n')[1:-1])
    return content.strip('` \n')


class TimeConverter(commands.Converter):

    @staticmethod
    def unit_getter(unit):
        if unit in ["s", "sec", "secs", "second", "seconds"]:
            return 1, "second"
        if unit in ["m", "min", "mins", "minute", "minutes"]:
            return 60, "minute"
        if unit in ["h", "hr", "hrs", "hour", "hours"]:
            return 3600, "hour"
        if unit in ["d", "day", "days"]:
            return 86_000, "day"
        if unit in ["w", "wk", "wks", "week", "weeks"]:
            return 604_800, "week"
        if unit in ["mth", "mths", "mos", "month", "months"]:
            return 2_580_000, "month"
        if unit in ["y", "yr", "yrs", "year", "years"]:
            return 31_390_000, "month"
        else:
            return None, None

    async def convert(self, ctx, argument):
        reg = re.compile("([0-9]+)([a-zA-Z]+)")
        time, unit = reg.match(argument).groups()
        unit_time, unit_name = self.unit_getter(unit.lower())
        seconds = unit_time * time
        return fmt_time(unit_name, unit_time, time, seconds)


def format_dt(dt: datetime.datetime, /, style: Optional[struct.TimestampStyle] = None) -> str:
    if style is None:
        return f'<t:{int(dt.timestamp())}>'

    return f'<t:{int(dt.timestamp())}:{style}>'


def user_friendly_dt(dt: datetime.datetime):
    return format_dt(dt, style='f') + f' ({format_dt(dt, style="R")})'
