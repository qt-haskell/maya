from itertools import starmap
from discord.ext import menus
from discord import Embed
from utils.constants import constants


class PageSource(menus.ListPageSource):

    def __init__(self, data, helpcommand):
        super().__init__(data, per_page=6)
        self.helpcommand = helpcommand

    def format_help_command(self, no, command):
        signature = self.helpcommand.get_command_signature(command)
        documentation = self.helpcommand.get_command_brief(command)
        return f'{no}. {signature}\n{documentation}'

    async def format_page(self, menu, entries):
        page = menu.current_page
        max_page = self.get_max_pages()
        starting_number = page * self.per_page + 1
        iterator = starmap(self.format_help_command, enumerate(entries, start=starting_number))
        page_content = "\n".join(iterator)
        embed = Embed(
            title=f"Help Command [{page + 1}/{max_page}]",
            description=page_content,
            color=constants.default_color
        )
        author = menu.ctx.author
        embed.set_footer(text=f"Requested by {author}", icon_url=author.avatar)  # author.avatar in 2.0
        return embed
