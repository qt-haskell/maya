from PIL import Image

from utils.enums import Piece, Color


class ChessPiece:

    def __init__(self, color: Color, piece: Piece):
        self.color = color
        self.piece = piece

    def getImage(self):
        im = Image.open(
            f"./assets/pieces/{self.color.name.lower()}/{self.piece.name.lower()}.png").convert("RGBA")
        return im
