from PIL import Image

from models.ChessPiece import ChessPiece
from utils.enums import Color, Piece

defaultBoard = [
    [
        ChessPiece(Color.Black, Piece.Rook),
        ChessPiece(Color.Black, Piece.Knight),
        ChessPiece(Color.Black, Piece.Bishop),
        ChessPiece(Color.Black, Piece.Queen),
        ChessPiece(Color.Black, Piece.King),
        ChessPiece(Color.Black, Piece.Bishop),
        ChessPiece(Color.Black, Piece.Knight),
        ChessPiece(Color.Black, Piece.Rook),
    ],
    [
        ChessPiece(Color.Black, Piece.Pawn),
        ChessPiece(Color.Black, Piece.Pawn),
        ChessPiece(Color.Black, Piece.Pawn),
        ChessPiece(Color.Black, Piece.Pawn),
        ChessPiece(Color.Black, Piece.Pawn),
        ChessPiece(Color.Black, Piece.Pawn),
        ChessPiece(Color.Black, Piece.Pawn),
        ChessPiece(Color.Black, Piece.Pawn),
    ],
    [
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    ],
    [
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    ],
    [
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    ],
    [
        None,
        None,
        None,
        None,
        None,
        None,
        None,
        None,
    ],
    [
        ChessPiece(Color.White, Piece.Pawn),
        ChessPiece(Color.White, Piece.Pawn),
        ChessPiece(Color.White, Piece.Pawn),
        ChessPiece(Color.White, Piece.Pawn),
        ChessPiece(Color.White, Piece.Pawn),
        ChessPiece(Color.White, Piece.Pawn),
        ChessPiece(Color.White, Piece.Pawn),
        ChessPiece(Color.White, Piece.Pawn),
    ],
    [
        ChessPiece(Color.White, Piece.Rook),
        ChessPiece(Color.White, Piece.Knight),
        ChessPiece(Color.White, Piece.Bishop),
        ChessPiece(Color.White, Piece.Queen),
        ChessPiece(Color.White, Piece.King),
        ChessPiece(Color.White, Piece.Bishop),
        ChessPiece(Color.White, Piece.Knight),
        ChessPiece(Color.White, Piece.Rook),
    ],
]


class ChessBoard:

    def __init__(self):
        self.board = defaultBoard

    def getImage(self):
        board = Image.open("./assets/chessboard/board.png")

        for y, row in enumerate(self.board):
            for x, piece in enumerate(row):
                if piece is not None:
                    pieceImage = piece.getImage()
                    board.paste(pieceImage, (x * pieceImage.width, y * pieceImage.height), pieceImage)

        return board

    def move(self, initial, final):

        # print(initial, final)

        x1, y1 = initial
        x2, y2 = final

        self.board[y2][x2] = self.board[y1][x1]
        self.board[y1][x1] = None
