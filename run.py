from dotenv import load_dotenv
from discord.ext import commands

import os
import time
import humanize
import discord
import asyncpg

from typing import Dict
from functions.better_help import MyHelp

load_dotenv()
os.environ["JISHAKU_NO_UNDERSCORE"] = "True"
os.environ["JISHAKU_NO_DM_TRACEBACK"] = "True"
os.environ["JISHAKU_HIDE"] = "True"

runner = os.getenv('Token')
DB_PASS = os.getenv('DB_PASS')
locations = [
    '\\cogs'
]


class Bot(commands.Bot):

    def __init__(self):
        activity = discord.Game('wanna slide in my dms?')
        allowed_mentions = discord.AllowedMentions.none()

        intents = discord.Intents(
            guilds=True,
            members=True,
            bans=True,
            emojis=True,
            invites=True,
            voice_states=True,
            presences=True,
            guild_messages=True,
            guild_reactions=True,
            guild_typing=True,
            message_content=True,
            dm_messages=True,
            dm_reactions=True,
            dm_typing=True,
        )

        super().__init__(
            activity=activity,
            owner_ids=[546691865374752778, 852419718819348510],
            allowed_mentions=allowed_mentions,
            case_insensitive=True,
            intents=intents,
            help_command=MyHelp(),
            command_prefix=commands.when_mentioned_or('::'))

        self.afk: Dict = {}
        self.db = asyncpg.pool
        self.start_time = time
        self.developer_mode: bool = False
        self.timestamp = discord.utils.format_dt

    async def setup_hook(self) -> None:
        try:
            print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;36m{time.strftime('%c', time.localtime())} ─ Waking up\x1b[0m")
            print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;34m{time.strftime('%c', time.localtime())} ─ Establishing connection with my database.\x1b[0m")
            self.db = await asyncpg.create_pool(dsn=f'postgres://byte:{DB_PASS}@localhost:5432/byte')
            print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;32m{time.strftime('%c', time.localtime())} ─ Connection established successfully.\x1b[0m")
        except Exception as exception:
            print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0;1;31m{time.strftime('%c', time.localtime())} ─ Could not connect due to : {exception}\x1b[0m")

        print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;34m{time.strftime('%c', time.localtime())} ─ Loading all Extensions.\x1b[0m")
        try:
            await self.load_extension('jishaku')
            print(
                f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;32m{time.strftime('%c', time.localtime())} ─ Jsk loaded successfully.\x1b[0m")
        except Exception as exception:
            print(
                f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;31m{time.strftime('%c', time.localtime())} ─ {exception} : {exception}\n\x1b[0m")

        for cog in locations:
            for i in next(os.walk(os.getcwd() + cog), (None, None, []))[2][::-1]:
                try:
                    await self.load_extension(cog[1:] + '.' + i[:-3])
                except Exception as exception:
                    print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;31m{time.strftime('%c', time.localtime())} ─ {exception} : {exception}\n\x1b[0m")

        print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;32m{time.strftime('%c', time.localtime())} ─ Extensions Successfully Loaded.\x1b[0m")

    async def on_ready(self):
        print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;32m{time.strftime('%c', time.localtime())} ─ Logged in as {self.user} - {self.user.id}.\x1b[0m")

        # Fetch Database data to the bot cache
        afk_list = await self.db.fetch("SELECT * FROM afk")
        self.afk = {data["user_id"]: data["reason"] for data in afk_list}

    async def on_message(self, message: discord.Message):
        await self.wait_until_ready()
        afk_list = await self.db.fetch("SELECT * FROM afk")

        if message.author.id in self.afk:
            for user in afk_list:
                time = user["time"]
            current_time = discord.utils.utcnow() - time
            await message.reply(f"<:blurpleinvite:1000480013012959303> Welcome back.\n<:replyarrow:1000374996612436039>` ─ ` You were afk for : **{humanize.naturaldelta(current_time)}**")
            await self.db.execute("DELETE FROM afk WHERE user_id = $1", message.author.id)
            self.afk.pop(message.author.id)

        for pinged_user in message.mentions:
            if pinged_user.id in self.afk:
                for data in afk_list:
                    time = data["time"]
                    reason = data["reason"]
                    current_time = discord.utils.utcnow() - time
                await message.reply(f"<:blurplemuted:1000480136665235618> **{pinged_user}** has been afk :\n<:replygoing:1000475132000739328>` ─ ` for : {reason}\n<:replyarrow:1000374996612436039>` ─ ` since : {humanize.naturaldelta(current_time)}")

        await self.process_commands(message)


bot = Bot()

if __name__ == '__main__':
    try:
        bot.run(runner)
    except KeyboardInterrupt:
        print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0;1;31m{time.strftime('%c', time.localtime())} ─ Keyboard interrupt : closing the connection\x1b[0m")
    except discord.LoginFailure:
        print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0;1;31m{time.strftime('%c', time.localtime())} ─ Invalid token : closing the connection\x1b[0m")
    except discord.PrivilegedIntentsRequired:
        print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0;1;31m{time.strftime('%c', time.localtime())} ─ Missing intents : closing the connection\x1b[0m")
    except discord.ConnectionClosed as e:
        print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;36m{time.strftime('%c', time.localtime())} ─ Connection closed : {e.reason}|{e.code}\x1b[0m")
    except Exception as exc:
        raise exc
