import asyncio
import re
import json

from discord.ext import commands
from json import JSONDecodeError

from utils.constants import constants
from functions.common_utils import restart_bot
from functions.views.common_views import ConfirmView

import io
import time
import inspect
import discord
import traceback


class Developer(commands.Cog, name='developer', description='Developer related commands, mostly private'):

    @property
    def display_name(self):
        return 'Developer'

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;32m{time.strftime('%c', time.localtime())} ─ Developer Extension Successfully Loaded.\x1b[0m")

    @commands.command(name='sync', hidden=True, help='<:replyarrow:1000374996612436039> Synchronize slash commands to your guild')
    async def sync(self, ctx: commands.Context):
        if ctx.author.id not in constants.developers:
            return await ctx.reply("Congratulations! You played yourself. Wait? You can't...")

        try:
            await self.bot.tree.sync(guild=discord.Object(id=ctx.author.guild.id))
            await ctx.reply("Slash commands have been successfully synced!")

        except discord.Forbidden:
            try:
                await ctx.reply('Sorry, looks like i do not have the permission to sync commands!')
            except discord.Forbidden:
                await ctx.author.send('Sorry, looks like i do not have the permission to sync commands!')

    @commands.command(name='restart', hidden=True, help='<:replyarrow:1000374996612436039> Restart the bot')
    async def restart(self, ctx: commands.Context):
        if ctx.author.id not in constants.developers:
            return await ctx.reply("Congratulations! You played yourself. Wait? You can't...")

        view = ConfirmView(ctx.author.id)

        context = await ctx.reply(content='Please confirm the restart!', view=view)
        await view.wait()

        if view.value:
            msg = await context.edit(content='Restart confirmed!', view=None)
            await msg.add_reaction('✅')
            restart_bot()
        else:
            await context.delete()

    @commands.command(name='load', aliases=['add'], hidden=True, help='<:replyarrow:1000374996612436039> Load an extension')
    async def load(self, ctx: commands.Context, *, extension):
        if ctx.author.id not in constants.developers:
            return await ctx.reply("Congratulations! You played yourself. Wait? You can't...")

        extension = extension.capitalize()

        try:
            await self.bot.load_extension(f'cogs.{extension}')
            await ctx.reply(f'{extension} cog loaded!')
        except Exception as exc:
            await ctx.reply("```py\n" + "".join(traceback.format_exception(type(exc), exc, exc.__traceback__)) + "```")

    @commands.command(name='unload', aliases=['remove'], hidden=True, help='<:replyarrow:1000374996612436039> Remove an extension')
    async def unload(self, ctx: commands.Context, *, extension):
        if ctx.author.id not in constants.developers:
            return await ctx.reply("Congratulations! You played yourself. Wait? You can't...")

        extension = extension.capitalize()

        try:
            await self.bot.unload_extension(f"cogs.{extension}")
            await ctx.reply(f"{extension} cog unloaded!")
        except Exception as exc:
            await ctx.reply("```py\n" + "".join(traceback.format_exception(type(exc), exc, exc.__traceback__)) + "```")

    @commands.command(name='source', aliases=['src'], help='<:replyarrow:1000374996612436039> Source code for commands', hidden=True)
    async def source(self, ctx: commands.Context, *, command: str = None):
        if ctx.author.id not in constants.developers:
            return await ctx.reply("Congratulations! You played yourself. Wait? You can't...")

        if not command:
            return await ctx.send('Please supply a command to get the source of.')

        cmd = self.bot.get_command(command)

        if not cmd:
            return await ctx.send('Unknown command.')

        await ctx.reply(file=discord.File(io.BytesIO(bytes(inspect.getsource(cmd.callback), encoding='utf-8')), f'{cmd.cog.__module__.split(".")[-1]}.py'))

    @commands.command(name='regex', help='<:replyarrow:1000374996612436039> Matches text to the regex provided', hidden=True)
    async def regex(self, ctx: commands.Context, regex, *, text):
        if ctx.author.id not in constants.developers:
            return await ctx.reply("Congratulations! You played yourself. Wait? You can't...")

        try:
            # We place our expression inside a executor
            task = self.bot.loop.run_in_executor(None, re.search, regex, text)
            # Quit if the match takes longer than 10 seconds
            match = await asyncio.wait_for(task, timeout=10.0)

        except asyncio.TimeoutError:
            return await ctx.reply('Regex timed out...')
        else:
            if match:
                return await ctx.reply(
                    f'<:blurpleemployee:1000373603390476308> **Regex :**```{regex}```\n'
                    f'<:blurplesearch:1001158774310051961> **Match :** ```{text[match.start():match.end()]}```')
            if not match:
                return await ctx.reply('No match...')

    @commands.command(name='json', help='<:replyarrow:1000374996612436039> Formats the given json string', hidden=True)
    async def format_json(self, ctx: commands.Context, *, json_string):
        if ctx.author.id not in constants.developers:
            return await ctx.reply("Congratulations! You played yourself. Wait? You can't...")

        json_string = json_string.lstrip("```").rstrip("```").lstrip("json")
        replacements = {"'": '"', "False": "false", "True": "true", "None": "null"}

        for to_replace, replacement in replacements.items():
            json_string = json_string.replace(to_replace, replacement)

        try:
            parsed_json = json.loads(json_string)
        except JSONDecodeError as exc:
            await ctx.send(
                embed=discord.Embed(
                    title='<:blurpleemployee:1000373603390476308> Invalid JSON',
                    description=discord.utils.escape_markdown(json_string),
                ).add_field(name='Exception', value=exc)
            )
        else:
            pretty_json = json.dumps(parsed_json, indent=4)
            await ctx.send(f"```json\n{pretty_json}```")


async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(Developer(bot))

