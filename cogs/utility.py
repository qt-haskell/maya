import asyncio
import discord
import aiosqlite
import asyncpg
import humanize
import psutil
import time
import json
import io

from typing import Union, Optional
from discord.ext import commands
from discord.ext.commands import MissingPermissions

from utils.constants import constants
from utils.logger import log
from utils.crucial import total_lines, misc
from utils.flags import user_perms, user_badges
from functions.views.paginator import Simple

from urllib.parse import urlencode


class Utilities(commands.Cog, name='utility', description='<:replyarrow:1000374996612436039> General utility events and functions'):

    @property
    def display_name(self):
        return '<:blurpleemployee:1000373603390476308> Utility'

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;32m{time.strftime('%c', time.localtime())} ─ Utility Extension Successfully Loaded.")

    @commands.command(name='setup-suggestion', help='<:replyarrow:1000374996612436039> Setup the suggestion process', hidden=True)
    async def setup(self, ctx: commands.Context):
        channel = self.bot.get_channel(944617405286596608)
        guild = ctx.guild

        if channel is None:
            overwrites = {
                guild.default_role: discord.PermissionOverwrite(read_messages=False),
                guild.me: discord.PermissionOverwrite(read_messages=True)
            }

            post_channel = await guild.create_text_channel('📰-mails', overwrites=overwrites)
            embed = discord.Embed(
                title='Setup completed',
                description=f'{post_channel.name} has been created.',
                color=discord.Color.blurple())

        else:
            post_channel = channel
            embed = discord.Embed(
                title='Setup completed',
                description=f'{post_channel.name} already exists.',
                color=discord.Color.blurple())

        await ctx.reply(embed=embed)

    @setup.error
    async def setup_error(self, ctx: commands.Context, error):

        if isinstance(error, MissingPermissions):
            embed = discord.Embed(
                title='Setup Error',
                description="You don't have permissions to manage mod mails",
                color=discord.Color.blurple())

            await ctx.reply(embed=embed)

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        channel = self.bot.get_channel(944617405286596608)

        if not message.content.startswith('!'):
            if isinstance(message.channel, discord.DMChannel) and message.author != self.bot.user:
                try:
                    embed_before_confirm = discord.Embed(
                        title='Please confirm to send the suggestion',
                        description=f'{message.content}',
                        color=discord.Color.blurple())
                    embed = discord.Embed(
                        title='Suggestion received',
                        description=f'**Sent by:** {message.author}\n**UserID:** {message.author.id}\n\n**Suggestion:**\n{message.content} \n',
                        color=discord.Color.blurple())

                    if message.attachments:
                        file_url = message.attachments[0].url
                        embed.set_image(url=file_url)
                        embed_before_confirm.set_image(url=file_url)

                    confirm = await message.author.send(embed=embed_before_confirm)

                    await confirm.add_reaction(constants.accept)
                    await confirm.add_reaction(constants.decline)

                    def reaction_check(payload, user):
                        if user.bot:
                            pass
                        else:
                            return str(payload.emoji.name) in constants.payload_list

                    payload, user = await self.bot.wait_for('reaction_add', timeout=60.0, check=reaction_check)

                    if str(payload.emoji.name) == constants.payload_list[0]:
                        await confirm.delete()

                        await channel.send(embed=embed)

                        embed_reply = discord.Embed(
                            title='Suggestion sent',
                            description='Your message has been sent to developers.',
                            color=discord.Color.blurple())
                        await message.author.send(embed=embed_reply)

                    else:
                        await message.author.send(content='Suggestion aborted.')
                        await confirm.delete()

                except Exception as exc:
                    log(exc)
                    embed_reply = discord.Embed(
                        title='Something, went wrong... 😔',
                        description="I could not send your message to developers",
                        color=discord.Color.blurple())
                    await message.author.send(embed=embed_reply)
                except asyncio.TimeoutError:
                    pass

    @commands.command(name='answer-suggestion', help='<:replyarrow:1000374996612436039> Accept or decline a suggestion', hidden=True)
    async def answer(self, ctx: commands.Context, id: str, msg: str):
        id = int(id)
        message = msg
        user = await self.bot.fetch_user(id)

        if user is not None:
            embed_reply = discord.Embed(
                title=f'Reply from {ctx.author.display_name}',
                description=message,
                color=discord.Color.blurple())
            await user.send(embed=embed_reply)

            embed = discord.Embed(
                title='Message sent successfully',
                description=f'Message sent to {user.name}',
                color=discord.Color.blurple())
            embed.add_field(name='message.content', value=msg, inline=False)
            await ctx.reply(embed=embed)

        elif user is None:
            embed = discord.Embed(
                title='Something, went wrong... 😔',
                description=f'I cannot find the user with the id {id}',
                color=discord.Color.blurple())
            await ctx.reply(embed=embed)

    @commands.command(name='ping', aliases=['pong'], help='<:replyarrow:1000374996612436039> Returns the bot latency')
    async def ping(self, ctx: commands.Context):

        latency = str(round(self.bot.latency * 1000))
        embed = discord.Embed(color=constants.default_color, description=f'🏓 Latency: `{latency}ms`')

        await ctx.reply(embed=embed)

    @commands.group(name='tag', help='<:replyarrow:1000374996612436039> Create, Remove, List customs tags')
    async def tag(self, ctx: commands.Context):
        if ctx.invoked_subcommand is not None:
            return

        command_name = ctx.bot.get_command('tag')
        await ctx.send_help(command_name)

    @tag.command(name='find', aliases=['query', 'search', 'get'], help='<:replyarrow:1000374996612436039> Display a custom tag')
    async def find(self, ctx: commands.Context, tag_name):
        print(ctx.author.name + ' - ' + tag_name)
        db = await aiosqlite.connect(constants.tag_db_path)

        async with db.execute("SELECT tag_text, user_added FROM tag_database WHERE tag_name = ? AND guild_id = ?",
                              (tag_name, ctx.guild.id)) as cursor:
            async for tag_fetch_var in cursor:
                tag_content = tag_fetch_var
                tag_text = tag_content[0]
                tag_creator = await self.bot.fetch_user(tag_content[1])
                await ctx.reply(content=tag_text)

        await db.close()

    @tag.command(name='create', aliases=['touch'], help='<:replyarrow:1000374996612436039> Create an new tag')
    async def create(self, ctx: commands.Context, tag_name, *, tag_text):
        db = await aiosqlite.connect(constants.tag_db_path)

        if len(tag_text) > 4000:
            return await ctx.reply('Your tag is exceeding the message limit!')

        await db.execute(
            "INSERT OR ROLLBACK INTO tag_database (guild_id, user_added, tag_name, tag_text) VALUES (?,?,?,?)",
            (ctx.guild.id, ctx.author.id, tag_name, tag_text))
        await db.commit()

        embed = discord.Embed(title=f'Successfully created {tag_name}', color=constants.default_color)
        embed.add_field(name='Tag description:', value=tag_text)
        await ctx.reply(embed=embed)

        await db.close()

    @tag.command(name='remove', aliases=['rm', 'delete'], help='<:replyarrow:1000374996612436039> Remove an unused tag')
    async def remove(self, ctx: commands.Context, tag_name):
        db = await aiosqlite.connect(constants.tag_db_path)

        await db.execute("DELETE FROM tag_database WHERE guild_id = ? AND tag_name = ?", (ctx.guild.id, tag_name))
        await db.commit()

        embed = discord.Embed(title=f'Successfully deleted {tag_name}', color=constants.default_color)
        embed.add_field(name='Information', value=f'Creator: {ctx.message.author.mention}\nName: {tag_name}')
        await ctx.reply(embed=embed)

    @tag.command(name='list', aliases=['my-tags'], help='<:replyarrow:1000374996612436039> List your own custom tags')
    async def list(self, ctx: commands.Context):
        member = ctx.author
        index = 0

        embed = discord.Embed(title=f'Custom Tags made by {ctx.author.display_name}', description='', color=constants.default_color)

        db = await aiosqlite.connect(constants.tag_db_path)
        async with db.execute("SELECT tag_name, tag_text FROM tag_database WHERE guild_id = ? AND user_added = ?", (ctx.guild.id, member.id,)) as cursor:
            async for tag_fetch_name, tag_fetch_text, in cursor:
                index += 1
                tag_name = tag_fetch_name
                tag_text = tag_fetch_text

                embed.description += f'{index}# | Name: {tag_name}\n' or 'User has no custom tags'

            await ctx.reply(embed=embed)

        await db.close()

    @commands.command(name='info', aliases=['misc', 'about', 'stats'], help='Stats, lines, functions, classes')
    async def stats(self, ctx: commands.Context):
        path = 'C:\\Users\\user\\PycharmProjects\\maya'
        core_count = psutil.cpu_count()
        cpu_usage = psutil.cpu_percent()
        mem_per = psutil.virtual_memory().percent
        mem_gb = psutil.virtual_memory().available / 1024 ** 3
        ram_usage = psutil.Process().memory_full_info().uss / 1024 ** 2

        embed = discord.Embed(
            color=constants.default_color)
        embed.description = (
            f"```"
            f"> CPU Usage         :  {cpu_usage:.2f} %"
            f"\n> CPU Core Count    :  {core_count} Cores"
            f"\n> Memory Used       :  {ram_usage:.2f} Megabytes"
            f"\n> Memory Available  :  {mem_gb:.3f} GB [ {mem_per} % ]"
            f"\n> Total Classes     :  {await misc(path, '.py', 'class'):,}"
            f"\n> Total Functions   :  {await misc(path, '.py', 'def'):,}"
            f"\n> Total Lines       :  {await total_lines(path, '.py'):,}```")
        await ctx.reply(embed=embed)

    @commands.command(name='banner', aliases=['header'], help='<:replyarrow:1000374996612436039> View a users banner')
    async def banner(self, ctx: commands.Context, *, user: discord.Member = None):
        user = user or ctx.author
        fetched_user = await ctx.bot.fetch_user(user.id)

        if fetched_user.banner is None:
            return await ctx.reply(f"**{user}** does not have a banner.")

        embed = discord.Embed(color=constants.default_color)
        embed.set_image(url=fetched_user.banner.url)
        await ctx.reply(embed=embed)

    @commands.command(name='spotify', aliases=['spot'], help='<:replyarrow:1000374996612436039> Get information on what the user is listening to')
    async def spotify(self, ctx: commands.Context, *, user: Union[discord.Member, discord.User] = None):
        user = user or ctx.author
        view = discord.ui.View()
        try:
            spotify = discord.utils.find(lambda sp: isinstance(sp, discord.Spotify), user.activities)
        except:
            return await ctx.reply(f"`{user}` is not in this guild, I'm sorry.")
        if spotify is None:
            if user == ctx.author:
                return await ctx.reply("You are not listening to Spotify right now.")
            else:
                return await ctx.reply(f"**{user}** is not listening to any song on **Spotify** right now.")
        else:
            embed = discord.Embed(
                title=f"{user}'s Spotify Status",
                description=f"They are listening to [**{spotify.title}**]({spotify.track_url}) by - **{spotify.artist}**",
                color=constants.default_color)
            embed.add_field(
                name="Song Information :",
                value=f"<:replygoing:1000475132000739328> ` - ` **Name :** [**{spotify.title}**]({spotify.track_url})\n"
                      f"<:replygoing:1000475132000739328> ` - ` **Album :** {spotify.album}\n"
                      f"<:replyarrow:1000374996612436039> ` - ` **Duration :** \"{humanize.precisedelta(spotify.duration)}\"")
            embed.set_thumbnail(url=spotify.album_cover_url)
            view.add_item(discord.ui.Button(label="Listen on Spotify", url=spotify.track_url, emoji="<a:Spotify:993120872883834990>"))
            await ctx.reply(embed=embed, view=view)

    @commands.command(name="afk", help="<:replyarrow:1000374996612436039> Sets your status to afk.")
    async def afk(self, ctx: commands.Context, *, reason: Optional[str]):
        await ctx.reply(f"<:blurpledeafened:1000479936777310228> Your afk has been set. Please enjoy!")
        if not reason:
            reason = "Not Specified . . ."
        self.bot.afk[ctx.author.id] = reason
        query = "INSERT INTO afk VALUES ($1, $2, $3)"
        try:
            await self.bot.db.execute(query, ctx.author.id, reason, ctx.message.created_at)
        except asyncpg.UniqueViolationError:
            return

    @commands.command(name='raw', help='<:replyarrow:1000374996612436039> Get the JSON Data for a message')
    async def json(self, ctx: commands.Context, message: Optional[discord.Message]):
        message: discord.Message = getattr(ctx.message.reference, "resolved", message)
        if not message:
            return await ctx.reply(
                f"**{ctx.author}**, please :\n<:replygoing:1000475132000739328>` - ` reply to the message \n<:replygoing:1000475132000739328>` - ` enter the message id \n<:replyarrow:1000374996612436039>` - ` send the message link")
        try:
            message_data = await self.bot.http.get_message(message.channel.id, message.id)
        except discord.HTTPException as HTTP:
            raise commands.BadArgument("Oops! There's an error, please try again")
        json_data = json.dumps(message_data, indent=2)
        await ctx.reply(f"<:blurpleemployee:1000373603390476308> Here is your requested json file.", file=discord.File(io.StringIO(json_data), filename="Raw-Message-Data.json"))

    @commands.command(name='userinfo', aliases=['whois', 'user'], help='<:replyarrow:1000374996612436039> Returns basic user information')
    async def userinfo(self, ctx: commands.Context, *, user: discord.Member = None) -> Optional[discord.Message]:
        user = user or ctx.author
        user_avatar = user.display_avatar.url

        roles = ''
        for role in user.roles:
            if role is ctx.guild.default_role:
                continue
            roles = f'{roles} {role.mention}'
        if roles != '':
            roles = f'{roles}'

        fetched_user = await ctx.bot.fetch_user(user.id)
        permissions = user_perms(user.guild_permissions)
        if permissions:
            perms_ = f"{' **|** '}".join(permissions)

        activity = discord.utils.find(lambda act: isinstance(act, discord.CustomActivity), user.activities)
        activity_holder = f"`{discord.utils.remove_markdown(activity.name)}`" if activity and activity.name else f'`{user}` has no activity at the moment.'

        user_emb = discord.Embed(title=f"{user}'s Information", color=constants.default_color)
        user_emb.add_field(
            name="<:blurpleinvite:1000480013012959303> General Info :",
            value=f"<:replygoing:1000475132000739328> Name : {user.mention} \n" \
                  f"<:replygoing:1000475132000739328> Nickname : {(user.nick) or 'No nickname set'} \n" \
                  f"<:replygoing:1000475132000739328> Discriminator : `#{user.discriminator}` \n"
                  f"<:replyarrow:1000374996612436039> Identification No. : `{user.id}`")
        user_emb.add_field(
            name="<:blurpleemployee:1000373603390476308> Account Info :",
            value=f"<:replygoing:1000475132000739328> Join Position : `#{sorted(ctx.guild.members, key = lambda u: u.joined_at or discord.utils.utcnow()).index(user) + 1}`\n "\
                  f"<:replygoing:1000475132000739328> Created on : {self.bot.timestamp(user.created_at, style='D')} ({self.bot.timestamp(user.created_at, style='R')}) \n" \
                  f"<:replyarrow:1000374996612436039> Joined Guild on : {self.bot.timestamp(user.joined_at, style='D')} ({self.bot.timestamp(user.joined_at, style='R')})",
            inline=False)
        user_emb.set_thumbnail(url=user_avatar)

        guild_emb = discord.Embed(title=f"{user} in {ctx.guild}", color=constants.default_color)
        guild_emb.add_field(
            name="<:blurplecertifiedmoderator:1000373598407622676> Permissions Present :",
            value=f"<:replyarrow:1000374996612436039> {perms_}")
        guild_emb.add_field(
            name="<:blurplesettings:1000373596759265290> All Roles Present :",
            value=f"<:replyarrow:1000374996612436039> {roles}",
            inline=False)
        guild_emb.set_thumbnail(url=user_avatar)

        misc_emb = discord.Embed(title=f"{user}'s - Misc. Information", color=constants.default_color)
        misc_emb.add_field(
            name="<:blurplecertifiedmoderator:1000373598407622676> Badges Present :",
            value=f"<:replyarrow:1000374996612436039> {user_badges(user=user, fetch_user=fetched_user) if user_badges(user=user, fetch_user=fetched_user) else 'No Badges Present'}")
        misc_emb.add_field(
            name="<:blurpleimage:1000537030247325716> Accent Colours :",
            value=f"<:replygoing:1000475132000739328> Banner Colour : `{str(fetched_user.accent_colour).upper()}` \n" \
                  f"<:replyarrow:1000374996612436039> Guild Role Colour : `{user.color if user.color is not discord.Color.default() else 'Default'}`",
            inline=False)
        misc_emb.add_field(
            name="<:blurpleannouncements:1000537031849562123> Activity :",
            value=f"<:replyarrow:1000374996612436039> {activity_holder}",
            inline=False)
        misc_emb.set_thumbnail(url=user_avatar)

        pfp_emb = discord.Embed(
            title=f"{user}'s Profile Picture",
            color=constants.default_color)
        pfp_emb.add_field(
            name='<:blurpleimage:1000537030247325716> Download Links :',
            value=f"<:replygoing:1000475132000739328> [**JPG Format**]({user.display_avatar.with_static_format('jpg')})\n"
                  f"<:replygoing:1000475132000739328> [**PNG Format**]({user.display_avatar.with_static_format('png')})\n"
                  f"<:replyarrow:1000374996612436039> [**WEBP Format**]({user.display_avatar.with_static_format('webp')})")
        pfp_emb.set_image(url=user_avatar)

        banner_emb = None
        if fetched_user.banner is None:
            embeds = [user_emb, guild_emb, misc_emb, pfp_emb]
            await Simple().start(ctx, embeds)
        else:
            banner_emb = discord.Embed(
                title=f"{user}'s Banner",
                color=constants.default_color)
            banner_emb.add_field(
                name='<:blurpleimage:1000537030247325716> Download Link:',
                value=f"<:replyarrow:1000374996612436039> [**Download Banner Here**]({fetched_user.banner.url})")
            banner_emb.set_image(url=fetched_user.banner.url)

            embeds = [user_emb, guild_emb, misc_emb, pfp_emb, banner_emb]
            await Simple().start(ctx, embeds)

    @commands.command(name='invite', help='<:replyarrow:1000374996612436039> Retrieve the invite URL for this bot.')
    async def invite(self, ctx: commands.Context, *perms: str):
        scopes = ('bot', 'applications.commands')
        permissions = discord.Permissions()

        for perm in perms:
            if perm not in dict(permissions):
                raise commands.BadArgument(f"Invalid permission: {perm}")

            setattr(permissions, perm, True)

        application_info = await self.bot.application_info()

        query = {
            "client_id": application_info.id,
            "scope": "+".join(scopes),
            "permissions": permissions.value
        }

        return await ctx.reply(
            f"<:blurplesettings:1000373596759265290> Link to invite this bot:\n<:replyarrow:1000374996612436039> <https://discordapp.com/oauth2/authorize?{urlencode(query, safe='+')}>"
        )


async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(Utilities(bot))
