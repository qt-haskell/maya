import time

from discord.ext import commands


class Meowth(commands.Cog, name='meowth', description='<:replyarrow:1000374996612436039> Bot utility and helpers'):

    @property
    def display_name(self):
        return '<:blurpleowo:1001164630128865391> Meowth'

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;32m{time.strftime('%c', time.localtime())} ─ Meowth Extension Successfully Loaded.")


async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(Meowth(bot))

