import difflib
import math
import discord

from discord.ext import commands
from functions.views.common_views import ConfirmView


class ErrorHandling(commands.Cog):

    @property
    async def display_name(self):
        return 'Error Handler'

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if message.author == self.bot:
            return

    @commands.Cog.listener()
    async def on_command_error(self, ctx, error):

        if isinstance(error, commands.CommandNotFound):
            await self.send_command_suggestion(ctx, ctx.invoked_with)
            return

        elif isinstance(error, commands.CommandOnCooldown):
            embed = discord.Embed(
                title='This command is  still on cooldown!',
                description=f'Try again after {math.ceil(error.retry_after)} seconds')
            await ctx.send(embed=embed, delete_after=math.ceil(error.retry_after))
        else:
            return

    async def send_command_suggestion(self, ctx: commands.Context, command_name: str) -> None:

        raw_commands = []
        for command in self.bot.walk_commands():
            if not command.hidden:
                raw_commands += (command.name, *command.aliases)

        if similar_command_data := difflib.get_close_matches(command_name, raw_commands, 1):
            similar_command_data = similar_command_data[0]
            similar_command = self.bot.get_command(similar_command_data)

            if not similar_command:
                return

            view = ConfirmView(ctx.message.author.id)

            misspelled_content = ctx.message.content
            suggestion_string = str(similar_command)

            e = discord.Embed(
                title='Command not found.',
                color=discord.Color.blurple())
            e.add_field(name='Command Suggestion', value=misspelled_content.replace(command_name, suggestion_string, 1))

            context = await ctx.send(embed=e, view=view)

            await view.wait()
            if view.value:
                await context.delete()
                await ctx.send_help(similar_command)
            else:
                await context.delete()


async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(ErrorHandling(bot))
