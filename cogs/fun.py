import aiohttp
import discord
import random
import time

from discord import Embed
from discord.ext import commands

from functions.views.tictactoe import ttt_game
from utils.constants import constants
from utils import crucial

error_titles = [
    "Not gonna happen.",
    "Not likely.",
    "Naw.",
    "Nah.",
    "Certainly not.",
    "Nuh-uh.",
]


class Fun(commands.Cog, name='fun', description='<:replyarrow:1000374996612436039> Funny commands and functionality'):

    @property
    def display_name(self):
        return '<:blurplepartner:1000373600232144976> Fun'

    def __init__(self, bot):
        self.bot = bot
        self.cats = ["ᓚᘏᗢ", "ᘡᘏᗢ", "🐈", "ᓕᘏᗢ", "ᓇᘏᗢ", "ᓂᘏᗢ", "ᘣᘏᗢ", "ᕦᘏᗢ", "ᕂᘏᗢ"]
        self.error_titles = [
            "Not gonna happen.",
            "Not likely.",
            "Naw.",
            "Nah.",
            "Certainly not.",
            "Nuh-uh.",
        ]
        self.hs = {
            "aquarius": {
                "name": "Aquarius",
                "emoji": ":aquarius:",
                "date_range": "Jan 20 - Feb 18",
            },
            "pisces": {
                "name": "Pisces",
                "emoji": ":pisces:",
                "date_range": "Feb 19 - Mar 20",
            },
            "aries": {
                "name": "Aries",
                "emoji": ":aries:",
                "date_range": "Mar 21 - Apr 19",
            },
            "taurus": {
                "name": "Taurus",
                "emoji": ":taurus:",
                "date_range": "Apr 20 - May 20",
            },
            "gemini": {
                "name": "Gemini",
                "emoji": ":gemini:",
                "date_range": "May 21 - Jun 20",
            },
            "cancer": {
                "name": "Cancer",
                "emoji": ":cancer:",
                "date_range": "Jun 21 - Jul 22",
            },
            "leo": {
                "name": "Leo",
                "emoji": ":leo:",
                "date_range": "Jul 23 - Aug 22",
            },
            "virgo": {
                "name": "Virgo",
                "emoji": ":virgo:",
                "date_range": "Aug 23 - Sep 22",
            },
            "libra": {
                "name": "Libra",
                "emoji": ":libra:",
                "date_range": "Sep 23 - Oct 22",
            },
            "scorpio": {
                "name": "Scorpio",
                "emoji": ":scorpius:",
                "date_range": "Oct 23 - Nov 21",
            },
            "sagittarius": {
                "name": "Sagittarius",
                "emoji": ":sagittarius:",
                "date_range": "Nov 22 - Dec 21",
            },
            "capricorn": {
                "name": "Capricorn",
                "emoji": ":capricorn:",
                "date_range": "Dec 22 - Jan 19",
            },
        }

    @commands.Cog.listener()
    async def on_ready(self):
        print(
            f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;32m{time.strftime('%c', time.localtime())} ─ Fun Extension Successfully Loaded.\x1b[0m")

    @commands.command(name='catify', help='<:replyarrow:1000374996612436039> Catiefies your display name')
    async def catify(self, ctx: commands.Context) -> None:
        display_name = ctx.author.display_name

        if len(display_name) > 26:
            embed = Embed(
                title=random.choice(error_titles),
                description=(
                    "Your display name is too long to be catified! "
                    "Please change it to be under 26 characters."
                ), color=discord.Color.blurple()
            )
            await ctx.reply(embed=embed)
            return

        else:
            try:
                display_name += f' {random.choice(self.cats)}'
                await ctx.author.edit(nick=display_name)
                await ctx.reply(content=f'Your catified nickname is: `{display_name}`')
            except discord.Forbidden:
                await ctx.reply(
                    content=f'Sorry, I cannot change your nickname due to my current lack of powers, but here is your catified nickname if you want to change it yourself: `{display_name}`')

    @commands.command(name='echo', aliases=['print', 'printf'],
                      help='<:replyarrow:1000374996612436039> Send a webhook to mimic another user')
    @commands.bot_has_guild_permissions(manage_webhooks=True)
    async def echo(self, ctx: commands.Context, user: discord.Member, *, message: str):
        if not user:
            return await ctx.reply('You need to provide a user.')
        if not message:
            return await ctx.reply('You need to provide a message.')
        try:
            webhook = await crucial.fetch_webhook(ctx.channel)
            thread = discord.utils.MISSING

            if isinstance(ctx.channel, discord.Thread):
                thread = ctx.channel

            await webhook.send(message, avatar_url=user.display_avatar.url, username=user.display_name, thread=thread)
        except discord.Forbidden:
            try:
                await ctx.author.send(content="I don't have `manage_webhooks` Permissions")
            except discord.Forbidden:
                await ctx.send(content="I don't have `manage_webhooks` Permissions")

    @commands.command(name='meme', help='<:replyarrow:1000374996612436039> Query a random meme from reddit')
    async def meme(self, ctx: commands.Context):
        embed = discord.Embed(title='Lichess | Meme Source', color=constants.default_color)

        async with aiohttp.ClientSession() as cs:
            async with cs.get('https://www.reddit.com/r/memes/new.json?sort=hot') as r:
                res = await r.json()
                embed.set_image(url=res['data']['children'][random.randint(0, 25)]['data']['url'])
                await ctx.reply(embed=embed)

    @commands.command(name='zodiac', aliases=['zs'], help='<:replyarrow:1000374996612436039> Get list of all zodiac signs')
    async def horoscope_list(self, ctx: commands.Context):
        content = discord.Embed(
            color=constants.default_color,
            title="Zodiac signs",
            description="\n".join(
                f"{sign['emoji']} **{sign['name']}**: {sign['date_range']}"
                for sign in self.hs.values()
            ),
        )
        return await ctx.send(embed=content)

    @commands.command(name='clap', help='<:replyarrow:1000374996612436039> Add a clap emoji between words')
    async def clap(self, ctx: commands.Context, *sentence):
        await ctx.send(" 👏 ".join(sentence) + " 👏")

    @commands.command(name='tictactoe', aliases=['ttt'], help='<:replyarrow:1000374996612436039> Play tic tac toe with friends')
    async def tictactoe(self, ctx: commands.Context, player1: discord.Member = None, player2: discord.Member = None):
        if player1 and player2:
            await ctx.send(
                f"Tic Tac Toe: {player1.mention} vs {player2.mention}," f" {player1.display_name} goes first",
                view=ttt_game(player1, player2),
            )
        elif player1:
            await ctx.send(
                f"Tic Tac Toe: {ctx.author.mention} vs {player1.mention}," f" {ctx.author.display_name} goes first",
                view=ttt_game(ctx.author, player1),
            )
        else:
            await ctx.send(
                "Tic Tac Toe: anyone vs anyone, person to click a button first goes first",
                view=ttt_game(None, None),
            )


async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(Fun(bot))
