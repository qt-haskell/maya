from utils.lichess import getLichessBoard
from utils.constants import constants
from discord.ext import commands
from discord import app_commands
from typing import Literal

import time
import discord


class Lichess(commands.Cog,  name='lichess', description='<:replyarrow:1000374996612436039> Lichess events and functions'):

    @property
    def display_name(self):
        return 'Lichess'

    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print(f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;32m{time.strftime('%c', time.localtime())} ─ Lichess Extension Successfully Loaded.\x1b[0m")

    @app_commands.command(name='get-board', description='Watch a friends game live inside discord')
    @app_commands.guilds(constants.testing_guild, constants.sod_guild)
    async def getBoard(self, interaction: discord.Interaction, _id: str, side: Literal['black', 'white']):

        isGameEnd = False
        firstTime = True

        if len(_id) > 8:
            gameId = _id[0:8]
        else:
            gameId = _id

        while not isGameEnd:

            status, isGameEnd, board = await getLichessBoard(gameId, side)  # player_one, player_two

            if status:
                if isGameEnd:
                    if firstTime:
                        embed = discord.Embed(
                            title=f'Game: {gameId} Side: {side}', color=discord.Color.blurple())
                        embed.set_image(url=board)
                        await interaction.response.send_message('The game has ended! GIF of the game coming up!', embed=embed, ephemeral=False)
                    else:
                        embed = discord.Embed(
                            title=f'Game: {gameId} Side: {side}', color=discord.Color.blurple())
                        embed.set_image(url=board)
                        await interaction.edit_original_message(content='The game has ended! GIF of the game coming up!', embed=embed)

                else:
                    if firstTime:
                        embed = discord.Embed(title=f'Game: {gameId}', color=discord.Color.blurple(), description=board)
                        # embed.add_field(name='Active players', value=f'> **{player_one}** vs **{player_two}**')
                        await interaction.response.send_message(embed=embed, ephemeral=False)
                        firstTime = False
                    else:
                        embed = discord.Embed(title=f'Game: {gameId}', color=discord.Color.blurple(), description=board)
                        # embed.add_field(name='Active players', value=f'> **{player_one}** vs **{player_two}**')
                        await interaction.edit_original_message(embed=embed)

            else:
                await interaction.response.send_message(f'An Error Occurred\n```{str(board)}```', ephemeral=False)


async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(Lichess(bot))
