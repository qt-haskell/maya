import time

import discord

from discord.ext import commands

from utils.constants import constants
from functions.views.paginator import inspect_user_formatter, inspection_formatter

from typing import Optional, Union, List, Dict, Tuple, Generator, TypeVar
T = TypeVar('T')


class Moderation(commands.Cog, name='moderation', description='<:replyarrow:1000374996612436039> Moderation events and functions'):

    @property
    def display_name(self):
        return '<:blurplecertifiedmoderator:1000373598407622676> Moderation'

    # Chunks a list into chunks of a given size.
    @staticmethod
    def chunks(array: List[T], chunk_size: int) -> Generator[List[T], None, None]:
        for i in range(0, len(array), chunk_size):
            yield array[i:i + chunk_size]

    @staticmethod
    def apply_overwrites(
            permissions: Dict[str, Tuple[bool, str]],
            allow: int,
            deny: int,
            name: str):

        allow_p: discord.Permissions = discord.Permissions(allow)
        deny_p: discord.Permissions = discord.Permissions(deny)

        for key, value in dict(deny_p).items():
            if value and permissions[key][0]:
                permissions[key] = (False, f"it is the channel's {name} overwrite")

        for key, value in dict(allow_p).items():
            if value and not permissions[key][0]:
                permissions[key] = (True, f"it is the channel's {name} overwrite")

    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_ready(self):
        print(
            f"\x1b[0;1;37;40m > \x1b[0m \x1b[0;1;35m──\x1b[0m \x1b[0;1;32m{time.strftime('%c', time.localtime())} ─ Mod Extension Successfully Loaded.\x1b[0m")

    @commands.command(
        name='purge',
        aliases=['clear', 'clean'],
        help=(
                '<:replyarrow:1000374996612436039> Deletes multiple messages from the current channel, you can specify users that it will delete messages from. '
                'You can also specify the amount of messages to check.'))
    async def purge(self, ctx: commands.Context, amount: Optional[int] = 20, *users: discord.Member):
        if ctx.author.guild_permissions.manage_messages:

            def check(message):
                if (not users) or (message.author in users):
                    return True
                else:
                    return False

            messages = await ctx.channel.purge(limit=amount, check=check)
            users = [user.mention for user in users] if users else ['anyone']

            embed = discord.Embed(
                title=f'{len(messages)} messages deleted!',
                description='Deleted messages from ' + ', '.join(users),
                color=constants.default_color)
            await ctx.send(embed=embed)
        else:
            await ctx.reply('You do not have `manage_messages` permission')

    @commands.command(name='ban', help='<:replyarrow:1000374996612436039> Ban an user from your server, you can specify multiple users at once.')
    async def ban(self, ctx: commands.Context, users: commands.Greedy[discord.Member] = None, *, reason=None):
        if ctx.author.guild_permissions.ban_members:
            if not users:
                return await ctx.reply('Provide at least one argument.')
            if not reason:
                reason = 'My server my reasoning'

            for user in users:
                try:
                    await user.ban(reason=reason)
                except discord.Forbidden:
                    pass

            members = [user.mention for user in users]

            embed = discord.Embed(color=constants.default_color)
            embed.add_field(name='The following member(s) were banned:', value=', '.join(members))
            embed.add_field(name='Reason:', value=reason)
            embed.add_field(name='Moderator:', value=f'Banned by: {ctx.author.mention}')
            await ctx.send(embed=embed)
        else:
            await ctx.reply('You are missing `ban_members` permission')

    @commands.command(name='kick', help='<:replyarrow:1000374996612436039> Kick an user from your server, you can specify multiple users at once.')
    async def kick(self, ctx: commands.Context, users: commands.Greedy[discord.Member] = None, *, reason=None):
        if ctx.author.guild_permissions.kick_members:
            if not users:
                return await ctx.reply('Provide at least one argument.')
            if not reason:
                reason = 'My server my reasoning'

            for user in users:
                try:
                    await user.kick(reason=reason)
                except discord.Forbidden:
                    pass

            members = [user.mention for user in users]

            embed = discord.Embed(color=constants.default_color)
            embed.add_field(name='The following member(s) were banned:', value=', '.join(members))
            embed.add_field(name='Reason:', value=reason)
            embed.add_field(name='Moderator:', value=f'Banned by: {ctx.author.mention}')
            await ctx.send(embed=embed)
        else:
            await ctx.reply('You are missing `kick_members` permission')

    @commands.command(name='inspect', aliases=['resolve'], help='<:replyarrow:1000374996612436039> Resolve user ids into usernames')
    async def inspect(self, ctx: commands.Context, *ids: int):
        if len(ids) > 25:
            return ctx.reply("Only 25 at a time please!")
        rows = []
        for user_id in ids:
            user = self.bot.get_user(user_id)
            if user is None:
                try:
                    user = await self.bot.fetch_user(user_id)
                except discord.errors.NotFound:
                    user = None

            if user is None:
                rows.append(f"`{user_id}` -> ?")
            else:
                rows.append(f"`{user_id}` -> {user} {user.mention}")

        formatter = inspect_user_formatter(rows, per_page=25)
        menu = inspection_formatter(formatter)
        await menu.start(ctx)

    @commands.command(name='permtrace', aliases=['ptrace'], help='<:replyarrow:1000374996612436039> View channel permissions.')
    async def permtrace(
            self,
            ctx: commands.Context,
            channel: Union[discord.TextChannel, discord.VoiceChannel],
            *targets: Union[discord.Member, discord.Role]):

        member_ids = {target.id: target for target in targets if isinstance(target, discord.Member)}
        roles: List[discord.Role] = []

        for target in targets:
            if isinstance(target, discord.Member):
                roles.extend(list(target.roles))
            else:
                roles.append(target)

        roles = list(set(roles))

        permissions: Dict[str, Tuple[bool, str]] = {}

        if member_ids and channel.guild.owner_id in member_ids:
            # Is owner, has all perms
            for key in dict(discord.Permissions.all()).keys():
                permissions[key] = (True, f"<@{channel.guild.owner_id}> owns the server")
        else:
            is_administrator = False

            for key, value in dict(channel.guild.default_role.permissions).items():
                permissions[key] = (value, "it is the server-wide @everyone permission")

            for role in roles:
                for key, value in dict(role.permissions).items():
                    # Denying a permission does nothing if a lower role allows it
                    if value and not permissions[key][0]:
                        permissions[key] = (value, f"it is the server-wide {role.name} permission")

                # Then administrator handling
                if role.permissions.administrator:
                    is_administrator = True

                    for key in dict(discord.Permissions.all()).keys():
                        if not permissions[key][0]:
                            permissions[key] = (
                            True, f"it is granted by Administrator on the server-wide {role.name} permission")

            if not is_administrator:
                try:
                    maybe_everyone = channel._overwrites[0]  # type: ignore
                    if maybe_everyone.id == channel.guild.default_role.id:
                        self.apply_overwrites(permissions, allow=maybe_everyone.allow, deny=maybe_everyone.deny,
                                              name="@everyone")
                        remaining_overwrites = channel._overwrites[1:]  # type: ignore
                    else:
                        remaining_overwrites = channel._overwrites  # type: ignore
                except IndexError:
                    remaining_overwrites = channel._overwrites  # type: ignore

                role_lookup = {r.id: r for r in roles}

                def is_role(overwrite: discord.abc._Overwrites) -> bool:  # type: ignore
                    if discord.version_info >= (2, 0, 0):
                        return overwrite.is_role()
                    return overwrite.type == 'role'  # type: ignore

                def is_member(overwrite: discord.abc._Overwrites) -> bool:  # type: ignore
                    if discord.version_info >= (2, 0, 0):
                        return overwrite.is_member()
                    return overwrite.type == 'member'  # type: ignore

                # Handle denies
                for overwrite in remaining_overwrites:
                    if is_role(overwrite) and overwrite.id in role_lookup:
                        self.apply_overwrites(permissions, allow=0, deny=overwrite.deny,
                                              name=role_lookup[overwrite.id].name)

                # Handle allows
                for overwrite in remaining_overwrites:
                    if is_role(overwrite) and overwrite.id in role_lookup:
                        self.apply_overwrites(permissions, allow=overwrite.allow, deny=0,
                                              name=role_lookup[overwrite.id].name)

                if member_ids:
                    # Handle member-specific overwrites
                    for overwrite in remaining_overwrites:
                        if is_member(overwrite) and overwrite.id in member_ids:
                            self.apply_overwrites(permissions, allow=overwrite.allow, deny=overwrite.deny,
                                                  name=f"{member_ids[overwrite.id].mention}")
                            break

        # Construct embed
        description = f"<:blurpleannouncements:1000537031849562123> This is the permissions calculation for the following targets in {channel.mention}:\n"
        description += "\n".join(f"- {target.mention}" for target in targets)

        description += (
            "\nPlease note the reasons shown are the **most fundamental** reason why a permission is as it is. "
            "There may be other reasons that persist these permissions even if you change the things displayed."
        )

        embed = discord.Embed(color=constants.default_color, description=description)

        allows: List[str] = []
        denies: List[str] = []

        for key, value in permissions.items():
            if value[0]:
                allows.append(f"\N{WHITE HEAVY CHECK MARK} {key} (because {value[1]})")
            else:
                denies.append(f"\N{CROSS MARK} {key} (because {value[1]})")

        for chunk in self.chunks(sorted(allows) + sorted(denies), 8):
            embed.add_field(name="\u200b", value="\n".join(chunk), inline=False)

        await ctx.reply(embed=embed)


async def setup(bot: commands.Bot) -> None:
    await bot.add_cog(Moderation(bot))
